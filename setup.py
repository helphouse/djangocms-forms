#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup


readme = open('README.rst').read()

setup(
    name='djangocms-forms',
    version_format='{tag}',
    description="""The easiest and most flexible Django CMS Form builder w/ ReCaptcha v2 support!""",
    long_description=readme,
    author='helphouse.io',
    author_email='support@helphouse.io',
    url='https://bitbucket.org/helphouse/djangocms-forms/',
    packages=[
        'djangocms_forms',
    ],
    include_package_data=True,
    install_requires=[
        'django-appconf',
        'django-ipware',
        'jsonfield',
        'unidecode',
        'tablib',
        'hashids',
        'requests',
        'django-cms>=3.0',
    ],
    setup_requires=['setuptools-git-version'],
    license="BSD",
    zip_safe=False,
    keywords='djangocms-forms',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
)
